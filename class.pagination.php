<?php
	/*
	
		Pagination Class
		
		Copyright (c) 2013 j'programme.
		
		All rights reserved. Tous droits reserves. 모든 권리 보유.
		
		http://www.jprogramme.co.uk
		http://www.jprogramme.com
		
		uk_admin@jprogramme.com
		jh@govt.kr

	
	*/
	class Pagination 
	{
		var $current_page;
		var $total_pages;
		var $contents_per_page;
		var $query;
		var $additional_var;
		var $i;
		var $ordered_number_in_asc;
		
		static function initialise($cpp=10,$ordered_number_in_asc=1,$add_varname="") {
			$cp = $_GET[$add_varname."page"] ? $_GET[$add_varname."page"] : 1; 
			$an_instance = new Pagination;
			$an_instance->current_page = $cp;
			$an_instance->contents_per_page = $cpp;
			$an_instance->additional_var = $add_varname;
			$an_instance->ordered_number_in_asc = $ordered_number_in_asc;
			$an_instance->i = ($cp-1)*$cpp;
			
			return $an_instance;
		} 
		
		function getTotal($counting_query) {
			$rs = mysql_num_rows(mysql_query($counting_query));
			
			if(!$this->ordered_number_in_asc) {
				$this->i = ($rs - (($this->current_page - 1) * $this->contents_per_page)) + 1;
			}
			
			$this->total_pages = ceil($rs / $this->contents_per_page);
			$this->query = $counting_query;
			return $this->total_pages;
		}
		
		
		function limitedResult() {
			$limit_query = " limit ".(($this->current_page-1) * $this->contents_per_page).",".$this->contents_per_page;
			
			$rs = mysql_query($this->query.$limit_query);
			return $rs;
		}
		
		function tick() {
			if($this->ordered_number_in_asc) {
				$this->i++;
			} else {
				$this->i--;
			}
			return $this->i;
		}
		
		function getCurrentMaxPage() {
			$nextPages = $this->current_page + 4;
			if($nextPages > $this->total_pages) { $nextPages = $this->total_pages; }
			return $nextPages;
		}
		
		function getCurrentMinPage() {
			$prevPages = $this->current_page - 4;
			if($prevPages < 1) { $prevPages = 1; }
			return $prevPages;
		}
		
		function createLink($pageNum) {
			$args = explode("?",$_SERVER['REQUEST_URI']);
			if(strstr($args[1],$this->additional_var."page=")) {
				$query_string = $args[0]."?".str_replace($this->additional_var."page=".$this->current_page,$this->additional_var."page=".$pageNum, $args[1]);
			} else {
				if($args[1]) {
					$query_string = $args[0]."?".$args[1]."&".$this->additional_var."page=".$pageNum;
				} else {
					$query_string = $args[0]."?".$this->additional_var."page=".$pageNum;
				}
			}
			return $query_string;
		}
		
		function getCurrentPage() {
			return $this->current_page;
		}
		
		function nextPage() {
			if($this->current_page >= $this->total_pages) {
				return false;
			}
			return $this->current_page+1;
		}
		
		function prevPage() {
			if($this->current_page <= 1) {
				return false;
			}
			return $this->current_page-1;
		}
	}

?>